package com.tad.darsni;
import androidx.annotation.NonNull;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;
import java.util.HashMap;
public class MainActivity extends FlutterActivity {
   private static final String CHANNEL ="com.tad.darsni/battery";
   @Override
  public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
  super.configureFlutterEngine(flutterEngine);
    new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL)
        .setMethodCallHandler(
          (call, result) -> {
            if (call.method.equals("getMessage"))
            {
             //java.util.HashMap<String, String> arguments=call.arguments() ;
               // String name=arguments.get("name");
                result.success(getMessage());
            }
            else{
                
            }
            // This method is invoked on the main thread.
            // TODO
          }
        );
  }
    public static String getMessage() {
        return "Hello from Java";
    }
}
