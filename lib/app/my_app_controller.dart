import 'package:com.tad.darsni/core/enums/connectivity_status.dart';
import 'package:com.tad.darsni/core/services/base_controller.dart';
import 'package:com.tad.darsni/core/utils/general_util.dart';

class MyAppController extends BaseController {
  ConnectivityStatus connectionStatus = ConnectivityStatus.ONLINE;

  @override
  void onInit() {
    listenToConnectionStatus();
    super.onInit();
  }

  void listenToConnectionStatus() {
    connectivityService.connectivityStatusController.stream.listen((event) {
      connectionStatus = event;
    });
  }
}
