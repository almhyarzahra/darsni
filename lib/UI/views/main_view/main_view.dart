import 'package:carousel_slider/carousel_slider.dart';
import 'package:com.tad.darsni/UI/shared/colors.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_drawer.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.darsni/core/utils/general_util.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../shared/custom_widgets/custom_button.dart';
import '../../shared/utils.dart';
import 'main_controller.dart';

class MainView extends StatefulWidget {
  const MainView({super.key});

  @override
  State<MainView> createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  late MainController controller;
  @override
  void initState() {
    controller = Get.put(MainController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      key: controller.key,
      drawer: CustomDrawer(),
      body: ListView(
        children: [
          screenHeight(35).ph,
          Row(
            children: [
              IconButton(
                onPressed: () {
                  controller.key.currentState!.openDrawer();
                },
                icon: Icon(Icons.menu),
                iconSize: screenWidth(10),
              ),
              screenWidth(5).pw,
              Column(
                children: [
                  Container(
                    width: screenWidth(3),
                    decoration: BoxDecoration(
                        color: AppColors.mainBlueColor,
                        borderRadius: BorderRadius.circular(screenWidth(20))),
                    child: Image.asset(
                      "images/darsni.jpg",
                      height: screenHeight(10),
                      fit: BoxFit.contain,
                    ),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: "درسني",
                    textColor: AppColors.mainBlueColor,
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(20),
                  ),
                ],
              ),
            ],
          ),
          screenWidth(40).ph,
          Padding(
            padding: const EdgeInsetsDirectional.all(8.0),
            child: Card(
              elevation: 4,
              shadowColor: AppColors.mainBlueColor,
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                          width: screenWidth(2),
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  AppColors.mainGoldColor,
                                  AppColors.mainBlueColor,
                                ],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                tileMode: TileMode.clamp,
                              ),
                              // color: AppColors.mainBlueColor.withOpacity(0.7),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(screenWidth(10)))),
                          child: CustomText(
                            textType: TextStyleType.CUSTOM,
                            text: "آخر المحاضرات النازلة",
                            fontWeight: FontWeight.bold,
                          )),
                    ],
                  ),
                  screenWidth(20).ph,
                  Obx(() {
                    return CarouselSlider.builder(
                        itemCount: controller.items.length,
                        options: CarouselOptions(
                            reverse: false,
                            autoPlay: true,
                            height: screenWidth(2),
                            enlargeCenterPage: true,
                            viewportFraction: 0.8,
                            enlargeStrategy: CenterPageEnlargeStrategy.scale,
                            enlargeFactor: 0.15,
                            onPageChanged: (index, reason) {
                              controller.currentIndex.value = index;
                            }),
                        itemBuilder: (context, index, realIndex) {
                          return Column(
                            children: [
                              ClipRRect(
                                borderRadius:
                                    BorderRadius.circular(screenWidth(20)),
                                child: Image.asset(
                                  controller.items[index],
                                  fit: BoxFit.fill,
                                  width: screenWidth(1.5),
                                  height: screenWidth(2.5),
                                ),
                              ),
                              CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: controller.lecture[index],
                                textColor: AppColors.mainBlueColor,
                              )
                            ],
                          );
                        });
                  }),
                  Obx(() {
                    return Center(
                      child: SizedBox(
                        height: screenHeight(30),
                        child: ListView(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          children: [
                            DotsIndicator(
                              dotsCount: controller.items.length,
                              position: controller.currentIndex.value,
                              decorator: DotsDecorator(
                                  activeColor:
                                      AppColors.mainBlueColor.withOpacity(0.5),
                                  color: AppColors.mainGreyColor,
                                  activeSize:
                                      Size(screenWidth(20), screenWidth(40)),
                                  activeShape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          screenWidth(10)))),
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsetsDirectional.all(8.0),
            child: Card(
              elevation: 4,
              shadowColor: AppColors.mainBlueColor,
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                          width: screenWidth(2),
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  AppColors.mainGoldColor,
                                  AppColors.mainBlueColor,
                                ],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                tileMode: TileMode.clamp,
                              ),
                              //color: AppColors.mainBlueColor.withOpacity(0.7),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(screenWidth(10)))),
                          child: CustomText(
                            textType: TextStyleType.CUSTOM,
                            text: "آخر المحاضرات المحملة",
                            fontWeight: FontWeight.bold,
                          )),
                    ],
                  ),
                  screenWidth(20).ph,
                  Obx(() {
                    return CarouselSlider.builder(
                        itemCount: controller.items1.length,
                        options: CarouselOptions(
                            reverse: false,
                            autoPlay: true,
                            height: screenWidth(2),
                            enlargeCenterPage: true,
                            viewportFraction: 0.8,
                            enlargeStrategy: CenterPageEnlargeStrategy.scale,
                            enlargeFactor: 0.15,
                            onPageChanged: (index, reason) {
                              controller.currentIndex1.value = index;
                            }),
                        itemBuilder: (context, index, realIndex) {
                          return Column(
                            children: [
                              ClipRRect(
                                borderRadius:
                                    BorderRadius.circular(screenWidth(20)),
                                child: Image.asset(
                                  controller.items1[index],
                                  fit: BoxFit.fill,
                                  width: screenWidth(1.5),
                                  height: screenWidth(2.5),
                                ),
                              ),
                              CustomText(
                                textType: TextStyleType.CUSTOM,
                                text: controller.lecture1[index],
                                textColor: AppColors.mainBlueColor,
                              )
                            ],
                          );
                        });
                  }),
                  Obx(() {
                    return Center(
                      child: SizedBox(
                        height: screenHeight(30),
                        child: ListView(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          children: [
                            DotsIndicator(
                              dotsCount: controller.items1.length,
                              position: controller.currentIndex1.value,
                              decorator: DotsDecorator(
                                  activeColor:
                                      AppColors.mainBlueColor.withOpacity(0.5),
                                  color: AppColors.mainGreyColor,
                                  activeSize:
                                      Size(screenWidth(20), screenWidth(40)),
                                  activeShape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          screenWidth(10)))),
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
                ],
              ),
            ),
          ),
          Row(
            children: [
              Container(
                  width: screenWidth(2),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          AppColors.mainGoldColor,
                          AppColors.mainBlueColor,
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        tileMode: TileMode.clamp,
                      ),
                      //color: AppColors.mainBlueColor.withOpacity(0.7),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(screenWidth(10)))),
                  child: CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: "آخر الأخبار",
                    fontWeight: FontWeight.bold,
                  )),
            ],
          ),
          SizedBox(
            height: screenHeight(4),
            child: ListView.builder(
              itemCount: controller.news.length,
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  elevation: 4,
                  shadowColor: AppColors.mainBlueColor,
                  child: Container(
                    width: screenWidth(2),
                    child: Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(screenWidth(20)),
                          child: Image.asset(
                            controller.items[index],
                            height: screenHeight(8),
                          ),
                        ),
                        screenWidth(20).ph,
                        CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: controller.news[index],
                          textColor: AppColors.mainBlackColor,
                          overflow: TextOverflow.ellipsis,
                        ),
                        // screenWidth(40).ph,
                        CustomButton(
                          text: "التفاصيل",
                          onPressed: () {},
                          textSize: screenWidth(35),
                          circularBorder: screenWidth(20),
                          widthButton: 3,
                          heightButton: 15,
                          backgroundColor:
                              AppColors.mainBlueColor.withOpacity(0.7),
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    ));
  }
}
