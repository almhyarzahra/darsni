import 'package:com.tad.darsni/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainController extends BaseController {
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  RxList<String> items = <String>[
    "images/images.jpg",
    "images/images.jpg",
    "images/images.jpg"
  ].obs;
  RxList<String> lecture =
      <String>["برمجة المحاضرة 1", "برمجة المحاضرة 2", "برمجة المحاضرة 3"].obs;
  RxList<String> items1 = <String>[
    "images/lecture.png",
    "images/lecture.png",
    "images/lecture.png",
    "images/lecture.png"
  ].obs;
  RxList<String> lecture1 = <String>[
    "ويب المحاضرة 1",
    "ويب المحاضرة 2",
    "ويب المحاضرة 3",
    "ويب المحاضرة 4"
  ].obs;
  RxList<String> news = <String>[
    "صدور نتائج مادة البرمجة نظري",
    "صدور نتائج مادة البرمجة عملي ",
    "صدور نتائج مادة الويب نظري"
  ].obs;
  RxInt currentIndex = 0.obs;
  RxInt currentIndex1 = 0.obs;
}
