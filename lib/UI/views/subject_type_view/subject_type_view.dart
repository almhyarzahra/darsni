import 'package:com.tad.darsni/UI/shared/colors.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.darsni/UI/shared/utils.dart';
import 'package:com.tad.darsni/UI/views/courses_view/courses_view.dart';
import 'package:com.tad.darsni/UI/views/lectures_view/lectures_view.dart';
import 'package:com.tad.darsni/UI/views/subject_type_view/subject_type_controller.dart';
import 'package:com.tad.darsni/core/enums/subject_kind.dart';
import 'package:com.tad.darsni/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/enums/subject_type.dart';

class SubjectTypeView extends StatefulWidget {
  const SubjectTypeView(
      {super.key, required this.subjectType, required this.subjectName});
  final SubjectType subjectType;
  final String subjectName;

  @override
  State<SubjectTypeView> createState() => _SubjectTypeViewState();
}

class _SubjectTypeViewState extends State<SubjectTypeView> {
  late SubjectTypeController controller;
  @override
  void initState() {
    controller = Get.put(SubjectTypeController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.mainBlueColor,
        title: SizedBox(
          height: screenHeight(25),
          child: ListView(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            children: [
              CustomText(
                  textType: TextStyleType.CUSTOM,
                  text:
                      "${widget.subjectName}/${widget.subjectType == SubjectType.BASIC ? "نظري" : "عملي"}"),
            ],
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomButton(
              text: "المحاضرات",
              onPressed: () {
                Get.to(() => LecturesView(
                    subjectType: widget.subjectType,
                    subjectName: widget.subjectName,
                    subjectKind: SubjectKind.LECTURES));
              },
              circularBorder: screenWidth(20),
              backgroundColor: AppColors.mainBlueColor.withOpacity(0.7),
            ),
            screenWidth(20).ph,
            CustomButton(
              text: "الدورات",
              circularBorder: screenWidth(20),
              onPressed: () {
                Get.to(() => CoursesView(
                    subjectType: widget.subjectType,
                    subjectName: widget.subjectName,
                    subjectKind: SubjectKind.COURSES));
              },
              backgroundColor: AppColors.mainGreyColor,
            ),
            Image.asset("images/study.png")
          ],
        ),
      ),
    ));
  }
}
