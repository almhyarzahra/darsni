import 'package:com.tad.darsni/UI/shared/colors.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.darsni/UI/shared/utils.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:chewie/chewie.dart';

class VideoLectureView extends StatefulWidget {
  const VideoLectureView({
    Key? key, required this.lectureName,
  }) : super(key: key);
  // final String videoURL;
  final String lectureName;

  @override
  _VideoLectureViewState createState() => _VideoLectureViewState();
}

class _VideoLectureViewState extends State<VideoLectureView> {
  late VideoPlayerController _videoPlayerController;
  late ChewieController _chewieController;

  @override
  void initState() {
    super.initState();
    _videoPlayerController = VideoPlayerController.network(
        "https://cllllb.com/assets/img/club/2023/11/170003674156065724_861437017527465_7048239623259199580_n.mp4");
    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController,
      autoPlay: true,
      looping: true,
      showControls: true, // إظهار شريط التقدم وأزرار التحكم
      allowMuting: true, // السماح بالكتم
      allowPlaybackSpeedChanging: true, // السماح بتغيير سرعة التشغيل
      customControls: MaterialControls(), // استخدام واجهة تحكم مدمجة
    );
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    _chewieController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.mainBlueColor,
        title: SizedBox(
          height: screenHeight(25),
          child: ListView(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            children: [
              CustomText(textType: TextStyleType.CUSTOM, text:widget.lectureName ),
            ],
          ),
        ),
      ),
      body: Center(
        child: Chewie(
          controller: _chewieController,
        ),
      ),
    );
  }
}
