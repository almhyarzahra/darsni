import 'package:com.tad.darsni/UI/shared/utils.dart';
import 'package:com.tad.darsni/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/enums/subject_kind.dart';
import '../../../core/enums/subject_type.dart';
import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_text.dart';
import 'courses_controller.dart';

class CoursesView extends StatefulWidget {
  const CoursesView(
      {super.key,
      required this.subjectType,
      required this.subjectName,
      required this.subjectKind});
  final SubjectType subjectType;
  final String subjectName;
  final SubjectKind subjectKind;

  @override
  State<CoursesView> createState() => _CoursesViewState();
}

class _CoursesViewState extends State<CoursesView> {
  late CoursesController controller;
  @override
  void initState() {
    controller = Get.put(CoursesController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.mainBlueColor,
        title: SizedBox(
          height: screenHeight(25),
          child: ListView(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            children: [
              CustomText(
                  textType: TextStyleType.CUSTOM,
                  text:
                      "${widget.subjectName}/${widget.subjectType == SubjectType.BASIC ? "نظري" : "عملي"}/${widget.subjectKind == SubjectKind.LECTURES ? "المحاضرات" : "الدورات"}"),
            ],
          ),
        ),
      ),
      body: ListView(
        children: [
          screenWidth(20).ph,
          Padding(
            padding:
                EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
            child: InkWell(
              onTap: () {},
              child: Container(
                height: screenHeight(20),
                decoration: BoxDecoration(
                    color: AppColors.mainBlueColor.withOpacity(0.7),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(screenWidth(10)))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    CustomText(
                        textType: TextStyleType.CUSTOM, text: "2021-2022"),
                    CustomText(textType: TextStyleType.CUSTOM, text: "فصل1"),
                  ],
                ),
              ),
            ),
          ),
          screenWidth(20).ph,
          Padding(
            padding:
                EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
            child: InkWell(
              onTap: () {},
              child: Container(
                height: screenHeight(20),
                decoration: BoxDecoration(
                    color: AppColors.mainGreyColor,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(screenWidth(10)))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    CustomText(
                        textType: TextStyleType.CUSTOM, text: "2021-2022"),
                    CustomText(textType: TextStyleType.CUSTOM, text: "فصل2"),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
