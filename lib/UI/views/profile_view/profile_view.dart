import 'package:flutter/material.dart';

import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_text.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({super.key});

  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        // flexibleSpace: Container(
        //   decoration: BoxDecoration(
        //     gradient: LinearGradient(
        //       colors: [AppColors.mainGoldColor, AppColors.mainBlueColor],
        //       begin: Alignment.topLeft,
        //       end: Alignment.bottomRight,
        //       tileMode: TileMode.clamp,
        //     ),
        //   ),

        //   // decoration: BoxDecoration(
        //   //   gradient: LinearGradient(
        //   //     colors: [AppColors.mainGoldColor, AppColors.mainBlueColor],
        //   //     begin: Alignment.topLeft,
        //   //     end: Alignment.bottomRight,
        //   //     tileMode: TileMode.clamp,
        //   //   ),
        //   // ),
        // ),
        backgroundColor: AppColors.mainBlueColor,
        title: CustomText(
          textType: TextStyleType.CUSTOM,
          text: "حسابي",
        ),
      ),
    ));
  }
}
