import 'package:com.tad.darsni/UI/shared/colors.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.darsni/UI/views/login_view/login_view.dart';
import 'package:com.tad.darsni/UI/views/register_view/register_controller.dart';
import 'package:com.tad.darsni/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../main_view/main_view.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../../shared/custom_widgets/custom_text_button.dart';
import '../../shared/custom_widgets/custom_text_field.dart';
import '../../shared/utils.dart';

class RegisterView extends StatefulWidget {
  const RegisterView({super.key});

  @override
  State<RegisterView> createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  late RegisterController controller;
  @override
  void initState() {
    controller = Get.put(RegisterController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Form(
        key: controller.formKey,
        child: ListView(
          children: [
            Container(
              decoration: BoxDecoration(
                  color: AppColors.mainBlueColor,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(screenWidth(10)),
                      bottomRight: Radius.circular(screenWidth(10)))),
              child: Image.asset(
                "images/darsni.jpg",
                height: screenHeight(7),
                fit: BoxFit.contain,
              ),
            ),
            CustomText(
              textType: TextStyleType.CUSTOM,
              text: "الرجاء تعبئة الحقول التالية لإنشاء حساب جديد",
              textColor: AppColors.mainBlackColor,
              fontWeight: FontWeight.bold,
            ),
            Padding(
              padding:
                  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(10)),
              child: Column(
                children: [
                  CustomTextField(
                      controller: controller.nameController,
                      labelText: "اسم المستخدم",
                      validator: (value) {
                        return value!.isEmpty ? "الرجاء التحقق من الاسم" : null;
                      }),
                  screenWidth(30).ph,
                  CustomTextField(
                      controller: controller.phoneController,
                      typeInput: TextInputType.phone,
                      labelText: "رقم الموبايل",
                      validator: (value) {
                        return value!.isEmpty ? "الرجاء التحقق من الرقم" : null;
                      }),
                  screenWidth(30).ph,
                  CustomTextField(
                    controller: controller.emailController,
                    labelText: "البريد الالكتروني",
                    typeInput: TextInputType.emailAddress,
                    validator: (value) {
                      return value!.isEmpty || !GetUtils.isEmail(value.trim())
                          ? "الرجاء التحقق من الايميل"
                          : null;
                    },
                  ),
                  screenWidth(30).ph,
                  Obx(() {
                    return CustomTextField(
                      controller: controller.passwordController,
                      labelText: "كلمة المرور",
                      Obscure: controller.hidden.value,
                      fontWeight: FontWeight.bold,
                      letterSpacing: screenWidth(20),
                      suffixIcon: IconButton(
                          onPressed: () {
                            controller.replace();
                          },
                          icon: Icon(controller.hidden.isTrue
                              ? Icons.visibility_off_outlined
                              : Icons.visibility)),
                      validator: (value) {
                        return value!.isEmpty
                            // || !isPassword(value)
                            ? "الرجاء التحقق من كلمة المرور"
                            : null;
                      },
                    );
                  }),
                  screenWidth(30).ph,
                  CustomTextField(
                      controller: controller.cityController,
                      labelText: "المدينة",
                      validator: (value) {
                        return value!.isEmpty
                            ? "الرجاء التحقق من المدينة"
                            : null;
                      }),
                  screenWidth(30).ph,
                  CustomTextField(
                      controller: controller.univercityController,
                      labelText: "الجامعة",
                      validator: (value) {
                        return value!.isEmpty
                            ? "الرجاء التحقق من الجامعة"
                            : null;
                      }),
                  screenWidth(30).ph,
                  CustomTextField(
                      controller: controller.collageController,
                      labelText: "الكلية",
                      validator: (value) {
                        return value!.isEmpty
                            ? "الرجاء التحقق من الكلية"
                            : null;
                      }),
                  screenWidth(30).ph,
                  CustomTextField(
                      controller: controller.yearController,
                      typeInput: TextInputType.number,
                      labelText: "السنة الدراسية",
                      validator: (value) {
                        return value!.isEmpty
                            ? "الرجاء التحقق من السنة الدراسية"
                            : null;
                      }),
                  screenWidth(30).ph,
                  CustomButton(
                    circularBorder: screenWidth(25),
                    text: "إنشاء حساب",
                    onPressed: () {
                      Get.to(() => MainView());
                      //if (controller.formKey.currentState!.validate()) {}
                    },
                  ),
                  screenWidth(30).ph,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: " لديك حساب؟",
                        textColor: AppColors.mainBlueColor,
                      ),
                      CustomTextButton(
                        onPressed: () {
                          Get.off(() => LoginView());
                        },
                        text: "تسجيل الدخول",
                        colorText: AppColors.mainBlueColor,
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    ));
  }
}
