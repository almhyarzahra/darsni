import 'package:com.tad.darsni/UI/shared/colors.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_text.dart';
import 'package:flutter/material.dart';

class ContactUsView extends StatefulWidget {
  const ContactUsView({super.key});

  @override
  State<ContactUsView> createState() => _ContactUsViewState();
}

class _ContactUsViewState extends State<ContactUsView> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.mainBlueColor,
        title: CustomText(textType: TextStyleType.CUSTOM, text: "تواصل معنا"),
      ),
    ));
  }
}
