import 'package:com.tad.darsni/UI/shared/colors.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.darsni/UI/views/question_view/question_controller.dart';
import 'package:com.tad.darsni/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/utils.dart';

class QuestionView extends StatefulWidget {
  const QuestionView({super.key, required this.nameSubject});
  final String nameSubject;

  @override
  State<QuestionView> createState() => _QuestionViewState();
}

class _QuestionViewState extends State<QuestionView> {
  late QuestionController controller;
  @override
  void initState() {
    controller = Get.put(QuestionController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.mainBlueColor,
        title: SizedBox(
          height: screenHeight(25),
          child: ListView(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            children: [
              CustomText(
                  textType: TextStyleType.CUSTOM,
                  text: "الامتحانات/${widget.nameSubject}"),
            ],
          ),
        ),
      ),
      body: ListView(children: [
        Padding(
          padding: EdgeInsetsDirectional.symmetric(
              horizontal: screenWidth(20), vertical: screenWidth(20)),
          child: CustomText(
            textType: TextStyleType.CUSTOM,
            text: "كم يوجد نوع من سلاسل ال DNA؟",
            textColor: AppColors.mainBlackColor,
            fontSize: screenWidth(20),
          ),
        ),
        Column(
          children: controller.answers.map((element) {
            return Padding(
              padding: EdgeInsetsDirectional.symmetric(
                  horizontal: screenWidth(20), vertical: screenWidth(20)),
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(screenWidth(30)),
                    border: Border.all(
                      color: AppColors.mainBlueColor,
                    )),
                child: Row(
                  children: [
                    Obx(() {
                      return Radio<dynamic>(
                          //splashRadius: screenHeight(5),
                          value: element,
                          // activeColor: questionsController.correct.value == 0
                          //     ? AppColors.mainBlackColor
                          //     : questionsController.correct.value == 1
                          //         ? AppColors.mainBlueColor
                          //         : AppColors.mainRedColor,
                          groupValue: controller.idAnswer.value,
                          onChanged: (newValue) {
                            controller.idAnswer.value = newValue.toString();
                            // questionsController.correct.value = 0;
                            // questionsController.idAnswer.value =
                            //     newValue.toString();
                            // questionsController.groupValue.value = e.status!;
                          });
                    }),
                    CustomText(
                      textType: TextStyleType.CUSTOM,
                      text: element,
                      textColor: AppColors.mainBlackColor,
                    ),
                  ],
                ),
              ),
            );
          }).toList(),
        ),
        Padding(
          padding:
              EdgeInsetsDirectional.symmetric(horizontal: screenWidth(3.5)),
          child: CustomButton(
            circularBorder: screenWidth(20),
            text: "الإجابة الصحيحة",
            onPressed: () {},
            backgroundColor: AppColors.mainColorGreen,
          ),
        ),
        screenWidth(20).ph,
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            CustomButton(
              text: "السابق",
              onPressed: () {},
              widthButton: 5,
              backgroundColor: AppColors.mainGreyColor,
              circularBorder: screenWidth(20),
            ),
            CustomButton(
              text: "التالي",
              onPressed: () {},
              widthButton: 5,
              circularBorder: screenWidth(20),
            ),
          ],
        )
      ]),
    ));
  }
}
