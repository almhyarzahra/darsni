import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.darsni/UI/shared/utils.dart';
import 'package:com.tad.darsni/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../../shared/custom_widgets/custom_text_field.dart';
import 'coupons_controller.dart';

class CouponsView extends StatefulWidget {
  const CouponsView({super.key});

  @override
  State<CouponsView> createState() => _CouponsViewState();
}

class _CouponsViewState extends State<CouponsView> {
  late CouponsController controller;
  @override
  void initState() {
    controller = Get.put(CouponsController());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.mainBlueColor,
        title: CustomText(
          textType: TextStyleType.CUSTOM,
          text: "الكوبونات",
        ),
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.symmetric(
          horizontal: screenWidth(20),
        ),
        child: Padding(
          padding: EdgeInsetsDirectional.only(top: screenWidth(10)),
          child: ListView(
            children: [
              Card(
                elevation: 4,
                shadowColor: AppColors.mainBlueColor,
                child: Padding(
                  padding: EdgeInsetsDirectional.all(8.0),
                  child: Column(
                    children: [
                      Center(
                        child: CustomText(
                          textType: TextStyleType.CUSTOM,
                          text: "أدخل الكوبون الخاص بك لتفعيل إحدى المواد",
                          textColor: AppColors.mainBlueColor,
                          fontWeight: FontWeight.bold,
                          fontSize: screenWidth(20),
                        ),
                      ),
                      screenWidth(20).ph,
                      CustomTextField(
                          controller: controller.couponsController,
                          labelText: "الكوبون",
                          validator: (value) {
                            return value!.isEmpty
                                ? "الرجاء التحقق من الكوبون"
                                : null;
                          }),
                      screenWidth(15).ph,
                      CustomButton(
                        text: "تفعيل",
                        widthButton: 3,
                        onPressed: () {},
                        circularBorder: screenWidth(20),
                      ),
                    ],
                  ),
                ),
              ),
              Image.asset("images/coupons.png")
            ],
          ),
        ),
      ),
    ));
  }
}
