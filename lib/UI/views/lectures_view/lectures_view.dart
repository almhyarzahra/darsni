import 'package:com.tad.darsni/UI/shared/colors.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.darsni/UI/shared/utils.dart';
import 'package:com.tad.darsni/core/enums/subject_kind.dart';
import 'package:com.tad.darsni/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/enums/subject_type.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../attachments_view/attachments_view.dart';
import '../video_lecture_view/video_lecture_view.dart';
import 'lectures_controller.dart';

class LecturesView extends StatefulWidget {
  const LecturesView(
      {super.key,
      required this.subjectType,
      required this.subjectName,
      required this.subjectKind});
  final SubjectType subjectType;
  final String subjectName;
  final SubjectKind subjectKind;
  @override
  State<LecturesView> createState() => _LecturesViewState();
}

class _LecturesViewState extends State<LecturesView> {
  late LecturesController controller;
  @override
  void initState() {
    controller = Get.put(LecturesController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.mainBlueColor,
        title: SizedBox(
          height: screenHeight(25),
          child: ListView(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            children: [
              CustomText(
                  textType: TextStyleType.CUSTOM,
                  text:
                      "${widget.subjectName}/${widget.subjectType == SubjectType.BASIC ? "نظري" : "عملي"}/${widget.subjectKind == SubjectKind.LECTURES ? "المحاضرات" : "الدورات"}"),
            ],
          ),
        ),
      ),
      body: ListView(
        children: [
          screenWidth(20).ph,
          Padding(
            padding:
                EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
            child: InkWell(
              onTap: () {
                Get.defaultDialog(
                    title: "اختر ما تريد",
                    titleStyle: TextStyle(color: AppColors.mainBlueColor),
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        CustomButton(
                          text: "مشاهدة",
                          onPressed: () {
                            Get.back();
                            Get.to(() => VideoLectureView(
                                  lectureName: "تحليل سلاسل DNA",
                                ));
                          },
                          widthButton: 4,
                          circularBorder: screenWidth(20),
                        ),
                        CustomButton(
                          text: "ملحقات",
                          onPressed: () {
                            Get.back();
                            Get.to(() => AttachmentsView());
                          },
                          widthButton: 4,
                          circularBorder: screenWidth(20),
                        ),
                      ],
                    ));
              },
              child: Container(
                height: screenHeight(10),
                decoration: BoxDecoration(
                    color: AppColors.mainBlueColor.withOpacity(0.7),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(screenWidth(10)))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: screenWidth(1.2),
                      child: CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: "تحليل سلاسل DNA",
                        fontWeight: FontWeight.bold,
                        overflow: TextOverflow.ellipsis,
                        fontSize: screenWidth(20),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        CustomText(
                            textType: TextStyleType.CUSTOM,
                            text: "المحاضرة الأولى"),
                        CustomText(
                            textType: TextStyleType.CUSTOM, text: "21-9-2023"),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          screenWidth(20).ph,
          Padding(
            padding:
                EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
            child: InkWell(
              onTap: () {
                Get.defaultDialog(
                    title: "اختر ما تريد",
                    titleStyle: TextStyle(color: AppColors.mainBlueColor),
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        CustomButton(
                          text: "مشاهدة",
                          onPressed: () {
                            Get.back();
                            Get.to(() => VideoLectureView(
                                  lectureName: "سلوكيات ال DNA",
                                ));
                          },
                          widthButton: 4,
                          circularBorder: screenWidth(20),
                        ),
                        CustomButton(
                          text: "ملحقات",
                          onPressed: () {
                            Get.back();
                            Get.to(() => AttachmentsView());
                          },
                          widthButton: 4,
                          circularBorder: screenWidth(20),
                        ),
                      ],
                    ));
              },
              child: Container(
                height: screenHeight(10),
                decoration: BoxDecoration(
                    color: AppColors.mainGreyColor,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(screenWidth(10)))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: screenWidth(1.2),
                      child: CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: "سلوكيات ال DNA",
                        fontWeight: FontWeight.bold,
                        overflow: TextOverflow.ellipsis,
                        fontSize: screenWidth(20),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        CustomText(
                            textType: TextStyleType.CUSTOM,
                            text: "المحاضرة الثانية"),
                        CustomText(
                            textType: TextStyleType.CUSTOM, text: "23-9-2023"),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
