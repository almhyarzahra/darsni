import 'package:com.tad.darsni/UI/shared/colors.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.darsni/UI/shared/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../shared/custom_widgets/custom_button.dart';
import 'attachments_controller.dart';

class AttachmentsView extends StatefulWidget {
  const AttachmentsView({super.key});

  @override
  State<AttachmentsView> createState() => _AttachmentsViewState();
}

class _AttachmentsViewState extends State<AttachmentsView> {
  late AttachmentsController controller;
  @override
  void initState() {
    controller = Get.put(AttachmentsController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.mainBlueColor,
        title: CustomText(textType: TextStyleType.CUSTOM, text: "ملحقات"),
      ),
      body: ListView(children: [
        Padding(
          padding: EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
          child: GridView.count(
            childAspectRatio: 0.6,
            crossAxisCount: 3,
            crossAxisSpacing: 0,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: controller.attachments.map((element) {
              return Column(
                children: [
                  InkWell(
                    onTap: () {
                      Get.defaultDialog(
                          title: "اختر ما تريد",
                          titleStyle: TextStyle(color: AppColors.mainBlueColor),
                          content: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              CustomButton(
                                text: "مشاهدة",
                                onPressed: () {
                                  Get.back();
                                },
                                widthButton: 4,
                                circularBorder: screenWidth(20),
                              ),
                              CustomButton(
                                text: "تحميل",
                                onPressed: () {
                                  Get.back();
                                },
                                widthButton: 4,
                                circularBorder: screenWidth(20),
                              ),
                            ],
                          ));
                    },
                    child: Container(
                      width: screenWidth(7),
                      height: screenHeight(10),
                      decoration: BoxDecoration(
                        color: AppColors.mainBlueColor.withOpacity(0.7),
                        shape: BoxShape.circle,
                      ),
                      child: Icon(
                        Icons.school_rounded,
                        color: AppColors.mainWhiteColor,
                      ),
                    ),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: element,
                    textColor: AppColors.mainBlackColor,
                    overflow: TextOverflow.ellipsis,
                  )
                ],
              );
            }).toList(),
          ),
        ),
        Image.asset("images/book.png"),
      ]),
    ));
  }
}
