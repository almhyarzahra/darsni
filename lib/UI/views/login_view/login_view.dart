import 'package:bot_toast/bot_toast.dart';
import 'package:com.tad.darsni/UI/shared/colors.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_text_button.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.tad.darsni/UI/shared/utils.dart';
import 'package:com.tad.darsni/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../main_view/main_view.dart';
import '../../shared/custom_widgets/custom_button.dart';
import '../register_view/register_view.dart';
import 'login_controller.dart';

class LoginView extends StatefulWidget {
  const LoginView({super.key});

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  late LoginController controller;

  @override
  void initState() {
    controller = Get.put(LoginController());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Form(
        key: controller.formKey,
        child: ListView(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(screenWidth(8)),
                  bottomRight: Radius.circular(screenWidth(8))),
              child: Image.asset(
                "images/darsni.jpg",
              ),
            ),
            Padding(
              padding:
                  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(10)),
              child: Column(
                children: [
                  screenWidth(20).ph,
                  CustomTextField(
                      controller: controller.nameController,
                      labelText: "اسم المستخدم",
                      validator: (value) {
                        return value!.isEmpty ? "الرجاء التحقق من الاسم" : null;
                      }),
                  screenWidth(10).ph,
                  Obx(() {
                    return CustomTextField(
                      controller: controller.passwordController,
                      labelText: "كلمة المرور",
                      Obscure: controller.hidden.value,
                      fontWeight: FontWeight.bold,
                      letterSpacing: screenWidth(20),
                      suffixIcon: IconButton(
                          onPressed: () {
                            controller.replace();
                          },
                          icon: Icon(controller.hidden.isTrue
                              ? Icons.visibility_off_outlined
                              : Icons.visibility)),
                      validator: (value) {
                        return value!.isEmpty
                            // || !isPassword(value)
                            ? "الرجاء التحقق من كلمة المرور"
                            : null;
                      },
                    );
                  }),
                  screenWidth(16).ph,
                  CustomButton(
                    circularBorder: screenWidth(25),
                    text: "تسجيل الدخول",
                    onPressed: () async {
                      //if (controller.formKey.currentState!.validate()) {}
                      //Get.to(() => MainView());
                      // customLoader();
                      // String javaMessage =
                      //     await controller.getMessageFromJava('mhyar');
                      // BotToast.closeAllLoading();
                      // print(javaMessage);
                    },
                  ),
                  screenWidth(16).ph,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: "ليس لديك حساب؟",
                        textColor: AppColors.mainBlueColor,
                      ),
                      CustomTextButton(
                        onPressed: () {
                          Get.off(() => RegisterView());
                        },
                        text: "إنشاء حساب",
                        colorText: AppColors.mainBlueColor,
                      )
                    ],
                  )
                ],
              ),
            )
            // Container(
            //   height: screenHeight(2),
            //   decoration: BoxDecoration(
            //       color: AppColors.mainBlueColor,
            //       borderRadius: BorderRadius.only(
            //           bottomLeft: Radius.circular(screenWidth(8)),
            //           bottomRight: Radius.circular(screenWidth(8)))),
            //   child: Image.asset(
            //     "images/darsni.jpg",
            //   ),
            // )
          ],
        ),
      ),
    ));
  }
}
