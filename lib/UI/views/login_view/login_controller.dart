import 'package:com.tad.darsni/core/services/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter/services.dart';


class LoginController extends BaseController {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  RxBool hidden = true.obs;
  void replace() {
    hidden.isTrue ? hidden.value = false : hidden.value = true;
  }

  static const platform = MethodChannel('com.tad.darsni/battery');

   Future<String> getMessageFromJava(String name) async {
    try {
      final String message =
          await platform.invokeMethod('getMessage');
      return message;
    } catch (e) {
      print('Error: $e');
      return '';
    }
  }
}
