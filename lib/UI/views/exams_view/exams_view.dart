import 'package:com.tad.darsni/UI/shared/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_text.dart';
import '../question_view/question_view.dart';

class ExamsView extends StatefulWidget {
  const ExamsView({super.key});

  @override
  State<ExamsView> createState() => _ExamsViewState();
}

class _ExamsViewState extends State<ExamsView> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.mainBlueColor,
        title: CustomText(
          textType: TextStyleType.CUSTOM,
          text: "الامتحانات",
        ),
      ),
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsetsDirectional.symmetric(
                horizontal: screenWidth(20), vertical: screenWidth(20)),
            child: InkWell(
              onTap: () {
                Get.to(() => QuestionView(
                      nameSubject: "بيوانفورماتيك",
                    ));
              },
              child: Container(
                height: screenHeight(20),
                decoration: BoxDecoration(
                  color: AppColors.mainBlueColor.withOpacity(0.7),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(screenWidth(20))),
                ),
                child: Center(
                  child: CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: "بيوانفورماتيك",
                    fontSize: screenWidth(20),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsetsDirectional.symmetric(
                horizontal: screenWidth(20), vertical: screenWidth(200)),
            child: InkWell(
              onTap: () {
                Get.to(() => QuestionView(
                      nameSubject: "برمجة",
                    ));
              },
              child: Container(
                height: screenHeight(20),
                decoration: BoxDecoration(
                  color: AppColors.mainGreyColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(screenWidth(20))),
                ),
                child: Center(
                  child: CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: "برمجة",
                    fontSize: screenWidth(20),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
