import 'package:com.tad.darsni/UI/shared/colors.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.darsni/UI/shared/utils.dart';
import 'package:com.tad.darsni/UI/views/subjects_view/subjects_controller.dart';
import 'package:com.tad.darsni/core/enums/subject_type.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../subject_type_view/subject_type_view.dart';

class SubjectsView extends StatefulWidget {
  const SubjectsView({super.key});

  @override
  State<SubjectsView> createState() => _SubjectsViewState();
}

class _SubjectsViewState extends State<SubjectsView> {
  late SubjectsController controller;
  @override
  void initState() {
    controller = Get.put(SubjectsController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
          backgroundColor: AppColors.mainBlueColor,
          title: CustomText(textType: TextStyleType.CUSTOM, text: "المواد")),
      body: ListView(
        children: [
          InkWell(
            onTap: () {
              Get.defaultDialog(
                  title: "اختر القسم",
                  titleStyle: TextStyle(color: AppColors.mainBlueColor),
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      CustomButton(
                        text: "نظري",
                        onPressed: () {
                          Get.back();
                          Get.to(() => SubjectTypeView(
                                subjectName: "بيوانفورماتيك",
                                subjectType: SubjectType.BASIC,
                              ));
                        },
                        widthButton: 6,
                        circularBorder: screenWidth(20),
                      ),
                      CustomButton(
                        text: "عملي",
                        backgroundColor: AppColors.mainGreyColor,
                        onPressed: () {
                          Get.back();
                          Get.to(() => SubjectTypeView(
                                subjectName: "بيوانفورماتيك",
                                subjectType: SubjectType.SUB,
                              ));
                        },
                        widthButton: 6,
                        circularBorder: screenWidth(20),
                      ),
                    ],
                  ));
            },
            child: Card(
              elevation: 4,
              shadowColor: AppColors.mainBlueColor,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Flexible(
                    child: Container(
                      width: screenWidth(4.5),
                      child: CustomText(
                        textType: TextStyleType.CUSTOM,
                        text: "بيوانفورماتيك",
                        textColor: AppColors.mainGreyColor,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(screenWidth(5)),
                    child: Image.asset(
                      "images/lecture.png",
                      height: screenHeight(10),
                      width: screenWidth(4),
                    ),
                  ),
                  Flexible(
                    child: Container(
                      width: screenWidth(5),
                      child: Column(
                        children: [
                          CustomText(
                            textType: TextStyleType.CUSTOM,
                            text: "د.اسامة ناصر",
                            textColor: AppColors.mainBlueColor,
                            overflow: TextOverflow.ellipsis,
                          ),
                          CustomText(
                            textType: TextStyleType.CUSTOM,
                            text: "أ.أنس التدمري",
                            textColor: AppColors.mainBlueColor,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}
