import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_text.dart';
import 'package:com.tad.darsni/UI/views/login_view/login_view.dart';
import 'package:com.tad.darsni/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:com.tad.darsni/UI/shared/colors.dart';
import 'package:com.tad.darsni/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.tad.darsni/UI/shared/utils.dart';
import 'package:get/get.dart';

import '../../views/about_app_view/about_app_view.dart';
import '../../views/contact_us_view/contact_us_view.dart';
import '../../views/coupons_view/coupons_view.dart';
import '../../views/exams_view/exams_view.dart';
import '../../views/profile_view/profile_view.dart';
import '../../views/subjects_view/subjects_view.dart';

class CustomDrawer extends StatefulWidget {
  const CustomDrawer({super.key});

  @override
  State<CustomDrawer> createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: AppColors.mainBlackColor.withOpacity(0.5),
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(screenWidth(20)),
              topLeft: Radius.circular(screenWidth(20)))),
      width: screenWidth(1.5),
      height: screenHeight(2),
      child: Padding(
        padding: EdgeInsetsDirectional.symmetric(
            horizontal: screenWidth(20), vertical: screenWidth(20)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {
                Get.to(() => ProfileView());
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(
                    Icons.person,
                    color: AppColors.mainWhiteColor,
                    size: screenWidth(10),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: "حسابي",
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(20),
                  )
                ],
              ),
            ),
            screenHeight(30).ph,
            InkWell(
              onTap: () {
                Get.to(() => SubjectsView());
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(
                    Icons.subject_rounded,
                    color: AppColors.mainWhiteColor,
                    size: screenWidth(10),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: "المواد",
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(20),
                  )
                ],
              ),
            ),
            screenHeight(30).ph,
            InkWell(
              onTap: () {
                Get.to(() => ExamsView());
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(
                    Icons.text_snippet_rounded,
                    color: AppColors.mainWhiteColor,
                    size: screenWidth(10),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: "الامتحانات",
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(20),
                  )
                ],
              ),
            ),
            screenHeight(30).ph,
            InkWell(
              onTap: () {
                Get.to(() => CouponsView());
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(
                    Icons.receipt_long_rounded,
                    color: AppColors.mainWhiteColor,
                    size: screenWidth(10),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: "الكوبونات",
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(20),
                  )
                ],
              ),
            ),
            screenHeight(30).ph,
            InkWell(
              onTap: () {
                Get.to(() => ContactUsView());
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(
                    Icons.compress_outlined,
                    color: AppColors.mainWhiteColor,
                    size: screenWidth(10),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: "تواصل معنا",
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(20),
                  )
                ],
              ),
            ),
            screenHeight(30).ph,
            InkWell(
              onTap: () {
                Get.to(() => AboutAppView());
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(
                    Icons.app_settings_alt_rounded,
                    color: AppColors.mainWhiteColor,
                    size: screenWidth(10),
                  ),
                  CustomText(
                    textType: TextStyleType.CUSTOM,
                    text: "حول التطبيق",
                    fontWeight: FontWeight.bold,
                    fontSize: screenWidth(20),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
