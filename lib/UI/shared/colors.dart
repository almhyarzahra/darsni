import 'package:flutter/material.dart';

class AppColors {
  static const Color mainOrangeColor = Color.fromRGBO(252, 96, 17, 1);
  static const Color mainGreyColor = Color.fromRGBO(124, 125, 126, 1);
  static const Color mainWhiteColor = Color.fromRGBO(255, 255, 255, 1);
  static const Color mainBlackColor = Color.fromRGBO(0, 0, 0, 1);
  static const Color mainGrey2Color = Color.fromRGBO(212, 209, 209, 1);
  static const Color mainBlueColor = Color.fromRGBO(1, 55, 99, 1);
  static const Color mainRedColor = Color.fromRGBO(221, 75, 57, 1);
  static const Color mainColorGreen = Color.fromRGBO(0, 197, 105, 1);
  static const Color mainGoldColor = Color.fromRGBO(255, 200, 53, 1);
}
//255 200 53
//21 49 74